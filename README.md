# base

#### Table of Contents

1. [Description](#description)
1. [Setup](#setup)
    * [What base affects](#what-base-affects)
    * [Setup requirements](#setup-requirements)
1. [Usage](#usage)
1. [Limitations](#limitations)
1. [Development](#development)

## Description

This module provides a consistent base configuration for Emerging
Technology / IT Lab systems. We use it mostly with 64 bit
(x86_64/amd64) Debian Jessie and Ubuntu Trusty EC2 instances and Docker
containers, but it also has less tested support for RedHat / CentOS 7, Oracle
Linux, and Amazon Linux (again, on x86_64/amd64 architectures).

In addition, it has some support for Debian on armv7l (i.e. Raspbian
for Raspbery Pis) and Darwin (macOS Sierra; it may work on earlier
systems).

The module ensures that a base set of packages is installed, including

* bash
* bash-completion
* bc
* bzip2
* ca-certificates
* curl
* git
* gzip
* less
* logrotate
* lsof
* man-db
* openssh-server
* rsyslog
* tzdata
* unzip
* wget

Additional packages are installed depending on the type of image being
built; for example, EC2 images also include

* cloud-init
* ethtool
* iptables
* ntp
* ntpdate
* pciutils

(or their equivalents).

The AWS CLI and `s3cmd` tool are also installed.

## Setup

### What base affects

*base* sets up APT, YUM or HomeBrew repos, installs common packages,
and configures the timezone (default is PST8PDT) and locale (default is
en_US).

### Setup requirements

Puppet 4.x must already be installed for *base* to work. Extra Puppet
module requirements are defined in _metadata.json_ and _Puppetfile_, so
`librarian-puppet` should be used to ensure all module dependencies are
resolved.

## Usage

Configuration is managed by Hiera, using the data files under _data_
and the hierarchy defined in _hiera.yaml_.

If new packages are required they should be added to
`base::packages::install` - in _common.yaml_ if the package name is
generic, or in the appropriate YAML file for specific OSes or platforms.

To exclude packages for some platforms, add those package names to
`base::packages::uninstall` in the appropriate YAML file.

## Limitations

*base* has been tested on Debian Jessie, Ubuntu Trusty, and macOS
Sierra; basic testing has also been done on the other OSes.

## Development

Submit issues / merge requests on
[code.stanford.edu](https://code.stanford.edu/et-puppet/base/).





