# == Class: base::generic_apt
#
# Generic APT configuration for Emerging Technology systems.
#
# automatically included by base::ubuntu or base::debian
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016 The Board of Trustees of the Leland Stanford Junior
# University
#
class base::apt(
  $sources,
) {

  package {
    [
      'apt',
      'apt-transport-https',
    ]:
    ensure => latest,
  }

  class { 'apt':
    update  => {
      frequency => 'daily',
    },
    purge   => {
      'sources.list'   => true,
      'sources.list.d' => true,
    },
    sources => $sources,
  }

  apt::setting { 'conf-itlab':
    priority => 50,
    source   => "puppet:///modules/${module_name}/etc/apt/apt.conf.d/50itlab",
  }

  debconf { 'resolvconf/linkify-resolvconf':
    type  => 'boolean',
    value => 'true',
  }

}
